#!/usr/bin/env python

from distutils.core import setup

setup(name='PyXenApi', 
      version='0.2',
      description='XenForo API for Python',
      author='MaiNDaY',
      author_email='mainday.mdv@gmail.com',
      url='https://moondev.ru/',
      packages=['PyXenApi'],
      install_requires=['requests'],
     )