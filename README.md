# Getting started <br />
Installing the library: `pip install PyXenApi` <br/>
```python
from PyXenApi import XenForoAPI

xen = XenForoAPI('API_TOKEN', 'DOMAIN') # Creating an instance of the class
```
<br />
To create a token, go to the following path: https://yoursite.com/admin.php?api-keys/ and click add API key <br/>
In the "Key type" field, select "User Key", and enter the nickname of the user who has the necessary rights. Next, in the "Allowed scopes" field, put the "All scopes" checkbox and click "Save". Next, we pass the received token to the instance of the class with the first argument, and with the second argument, we pass the domain (without https://)


## Available methods
`GetMe` - Gets information about the current API user. <br />
```python
>>> xen.GetMe()
['Returns an array with the information of the user who owns the API key']
```
<br/>

`GetForumByID` - Gets information about the specified forum. <br />
```python
>>> xen.GetForumByID(1)
['Returns an array with information about the ID of the requested forum']
```
<br />

`GetNodes` - Gets the node tree. <br/>

```python
>>> xen.GetNodes(2)
['Returns an array with information about the requested node tree']
```
<br />

`GetPostByID` - Gets information about the specified post <br />

```python
>>> xen.GetPostByID(5)
['Returns an array with information about the requested message by ID']
```
<br />

`DeletePost` - Deletes the specified post. Default to soft deletion. <br />

```python
>>> xen.DeletePost(5, 'Test reason', 0, 0, 'Test alert')
['Deletes the specified post. Before using it, make sure that the user of the key has such rights']
```
This method takes the following arguments:<br/>
`post_id` - ID of the post, 
<br/>`reason` - reason for deletion,
<br/>`hard_delete` - physical deletion or not,
<br/>`author_alert` - send notification,
<br/>`author_alert_reason` - notification text. <br /> 

**0 - No, 1 - Yes.**
